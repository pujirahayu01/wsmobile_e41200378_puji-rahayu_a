
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget{
  @override

  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Beljar Routing'),
      ),
      body: Center(child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/about');
            },
            child: Text('Tap untuk ke AboutPage'),
            ),
            ElevatedButton( 
              onPressed: (){
                Navigator.pushNamed(context, '/halaman-404');
              },
              child: Text('Tap Halaman Lain'),
              ),
        ],
      ),
      ),
    );
  }
}

class AboutPage extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Tentang Aplikasi'),
      ),
      body: Center(
        child: ElevatedButton(onPressed: (){
          Navigator.pop(context);
        },
        child: Text('Kembali'),
        ),
        ),
    );
  }
}