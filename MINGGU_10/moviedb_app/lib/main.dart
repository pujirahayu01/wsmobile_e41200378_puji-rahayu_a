import 'package:flutter/material.dart';
import 'package:moviedb_app/utils/text.dart';
import 'package:moviedb_app/widgets/toprated.dart';
import 'package:moviedb_app/widgets/tv.dart';
import 'package:tmdb_api/tmdb_api.dart';
import 'widgets/trendingmovies.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(),
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          // brightness: Brightness.dark,
          primaryColor: Colors.purpleAccent),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List trendingmovies = [];
  List topratedmovies = [];
  List tv = [];
  final String apikey = '8a22c1b48be9e0b15fe982e9b309cc6c';
  final readaccesstoken =
      'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI4YTIyYzFiNDhiZTllMGIxNWZlOTgyZTliMzA5Y2M2YyIsInN1YiI6IjYyNjc0ZDgwMmIxMTNkMDBhNWM2ZmE2MyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.ioLIEeVAecOxyhEAQ8PNT07EEAanRq89txXZdEeECxQ';

  @override
  void initState() {
    loadmovies();
    super.initState();
  }

  loadmovies() async {
    TMDB tmdbmovie = TMDB(ApiKeys(apikey, readaccesstoken),
        logConfig: ConfigLogger(showLogs: true, showErrorLogs: true));

    Map trendingresult = await tmdbmovie.v3.trending.getTrending();
    Map topratedresult = await tmdbmovie.v3.movies.getTopRated();
    Map tvresult = await tmdbmovie.v3.tv.getPopular();

    setState(() {
      trendingmovies = trendingresult['results'];
      topratedmovies = topratedresult['results'];
      tv = tvresult['results'];
    });

    print(trendingresult);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: modified_text(
          text: 'MovieDB Application',
          color: Colors.white,
          size: 20,
        ),
      ),
      body: ListView(
        children: [
          TV(tv: tv),
          TopRated(toprated: topratedmovies),
          TrendingMovies(trending: trendingmovies)
        ],
      ),
    );
  }
}
