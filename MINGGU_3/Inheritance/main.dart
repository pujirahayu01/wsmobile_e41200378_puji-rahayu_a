import 'package:acara_9_minggu3/Inheritance/armor_titan.dart';
import 'package:acara_9_minggu3/Inheritance/attack_titan.dart';
import 'package:acara_9_minggu3/Inheritance/beast_titan.dart';
import 'package:acara_9_minggu3/Inheritance/human.dart';

import 'titan.dart';
void main(List<String> args) {
  armor_titan armon = new armor_titan();
  armon.powerPoint=3;
  armon.terjang();
  print(armon.powerPoint);

  attack_titan attack = new attack_titan();
  attack.powerPoint=2;
  attack.punch();
  print(attack.powerPoint);

  beast_titan beast = new beast_titan();
  beast.powerPoint = 4 ;
  beast.lempar();
  print(beast.powerPoint);

  human hum = new human();
  hum.powerPoint = 4;
  hum.killAlltitan();
  print(hum.powerPoint);
}