import 'bangun_datar.dart';
class Lingkaran extends BangunDatar{

  double phi = 3.14;
  double jari = 0;
  Lingkaran(double jari) {
    this.jari= jari;
  } 
  
  @override
  double keliling() {
    return 2 * phi * jari;
  }

  @override
  double luas() {
    return 3.14 * jari * jari;
  }


}