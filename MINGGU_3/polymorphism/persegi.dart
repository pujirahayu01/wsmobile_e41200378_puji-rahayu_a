import 'bangun_datar.dart';
class Persegi extends BangunDatar {
 
 double sisi = 0;

 //construktor
 Persegi(double sisi){
   this.sisi = sisi;
 }

 //rumus luas dan keliling
 @override
  double keliling() {
    // TODO: implement keliling
    return 4 * sisi;
  }

  @override
  double luas() {
    // TODO: implement luas
    return sisi*sisi;
  }
}