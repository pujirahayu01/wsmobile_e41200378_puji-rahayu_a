import 'bangun_datar.dart';

class Segitiga extends BangunDatar {
  double alas = 0;
  double tinggi = 0;
  double b = 0;

//construktor
Segitiga(double alas , double tinggi, double b){
  this.alas = alas;
  this.tinggi = tinggi;
  this.b = b;
}
@override
  double keliling() {
    // TODO: implement keliling
    return alas+tinggi+b;
  }
@override
  double luas() {
    // TODO: implement luas
    return 0.5 * alas * tinggi;
  }
  
}