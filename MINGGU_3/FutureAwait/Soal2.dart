void main(List<String> args) {
  print("Life");

  //memanggil nama method di future lalu memasukkan pesan, dan membua
 delayPrint("never flat").then((status){
   print(status);
 });
  print("is");
}


//int second, String message
Future delayPrint(String message){
  final duration = Duration(seconds: 2);
  return Future.delayed(duration).then((value) => message);
}