import 'dart:async';

import 'package:acara_9_minggu3/FutureAwait/future.dart';

Future<String> createOrderMessage() async{
  var order = await fetchUserOrder();

  return 'Your order is: $order';
}

Future<String> fetchUserOrder()=>

Future.delayed(Duration(seconds: 2),
() => 'Large Latte',
);

Future<void> main() async{
  print('Fetching User Order....');
  print(await createOrderMessage());
}