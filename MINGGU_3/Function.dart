void main(){
  tampilkan();
  print(munculkanangka());
  print(kalikanTiga(9));
  print(kalikan(4, 5));
  tampilkanangka(5);

  print(functionPerkalian(2, 3));
}

//fungsi 1
tampilkan(){
  print("Selamat Pagi DUnia");
}

//fungsi2
munculkanangka(){
  return 3;
}

//fungsi 3
kalikanTiga(angka){
  return angka *3;
}

//fungsi 4
kalikan(x,y){
  return x * y;
}

//fungsi 5
tampilkanangka(n1, {s1:45}){
  print(n1); //hasil akan 5 karena initialisasi 5 didalam value tampilkan
  print(s1); //hasil adalah 45 karena dari parameter diisi 45
}

//function parameter undefined
functionPerkalian(angka1, angka2){
  return angka1 * angka2;
}